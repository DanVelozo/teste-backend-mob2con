class AddAvatarToVisitors < ActiveRecord::Migration[6.0]
  def change
    add_column :visitors, :visitor_avatar, :string
  end
end
