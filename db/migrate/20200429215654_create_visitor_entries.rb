class CreateVisitorEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :visitor_entries do |t|
      t.datetime     :checkin_at
      t.datetime     :checkout_at
      t.text         :notes
      t.references   :visitor
      t.references   :retail_chain

      t.timestamps
    end
  end
end
