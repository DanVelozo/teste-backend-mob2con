puts "Creating Retail Chains"

retail_chain = RetailChain.create(name: "BIG Supermercado", cnpj: "03634698000195")
another_retail_chain = RetailChain.create(name: "Sonda Supermercado", cnpj: "01634632300092")

puts "RetailChain id: #{retail_chain.id} | name: #{retail_chain.name} | created successfully!"
puts "RetailChain id: #{another_retail_chain.id} | name: #{another_retail_chain.name} | created successfully!"

puts "\n"

#####################################

puts "Creating Users"

first_user  = User.create(name: "Usuário", username: "user",  password: "123456", role: "user",  retail_chain_id: retail_chain.id)
second_user = User.create(name: "Admin",   username: "admin", password: "123456", role: "admin", retail_chain_id: another_retail_chain.id)

puts "User id: #{first_user.id} | username: #{first_user.username} | password: #{first_user.password} | retail_chain_id: #{retail_chain.id} created successfully!"
puts "User id: #{second_user.id} | username: #{second_user.username} | password: #{second_user.password} | retail_chain_id: #{another_retail_chain.id} created successfully!"

puts "\n"

#####################################

puts "Creating Visitors"

first_visitor = Visitor.create(name: "Pedro Alvares Cabral", retail_chain_id: retail_chain.id)
second_visitor = Visitor.create(name: "João da Silva", retail_chain_id: another_retail_chain.id)

puts "Visitor id: #{first_visitor.id}  | name: #{first_visitor.name}  | retail_chain_id: #{retail_chain.id} created successfully!"
puts "Visitor id: #{second_visitor.id} | name: #{second_visitor.name} | retail_chain_id: #{another_retail_chain.id} created successfully!"

puts "\n"

puts "You're all set!"