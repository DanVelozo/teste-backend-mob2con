# Mob2Con - Teste Backend

* **Candidato:** Danilo Velozo
* **E-mail:** velozo.dan@gmail.com
*  **Github:** [https://github.com/danilovelozo](https://github.com/danilovelozo)


# Dependências

* Ruby 2.6.3
* Rails 6
* PostgreSQL
* ElasticSearch
* [Postman](https://www.postman.com/downloads/)


Para instalar o Ruby ([RVM](https://rvm.io/) ou [Rbenv](https://github.com/rbenv/rbenv)) e o [PostgreSQL](https://www.postgresql.org/), utilize este [guia de instalação](https://gorails.com/setup).


Para instalar o [Elasticsearch](https://www.elastic.co/elasticsearch/), utilize esses guias abaixo:

* **[Ubuntu](https://linuxize.com/post/how-to-install-elasticsearch-on-ubuntu-18-04/)**
* **[MacOS](https://www.elastic.co/guide/en/elasticsearch/reference/current/brew.html)**

Iniciar o Elasticsearch através dos comandos abaixo:
* `systemctl start elasticsearch` / **Ubuntu**

*  `brew services start elasticsearch` / **MacOS**


## Configurando Projeto

Instalar as dependências através do comando:

* `bundle install`

Alterar o `username` e `password` no arquivo `config/database.yml`:

```
default: &default
  adapter: postgresql
  encoding: unicode
  username: <seu-usuario>
  password: <sua-senha>
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  ```

Criar o banco de dados e `migrations`através do comando:

* `rails db:create db:migrate`

Popular com os dados de teste com o comando:

* `rails db:seed`

O `seed` vai criar 2 usuários de teste:

**Admin:**

```
usernamme: admin
password: 123456
```

**User:**

```
usernamme: user
password: 123456
```

Start no servidor através do comando:


```
rails s

```

URL Padrão:


```
http://localhost:3000

```

Para rodar os testes, execute o comando abaixo na raiz do projeto:

```
rspec

```

O `simplecov` vai gerar um report de cobertura de testes, que pode ser acessado através dos comandos a seguir, no terminal:

`open coverage/index.html` | **MacOS**

`xdg-open coverage/index.html` | **Ubuntu**


## Documentação API

A documentação pode ser acessada através do link abaixo:

[Documentação API](https://documenter.getpostman.com/view/1157404/SzfDx5DR?version=latest)


## Endpoint para Testes


`https://danvelozo-teste-backend-m2c.herokuapp.com/api/v1/`

---

Dúvidas, estou a disposição.

Obrigado!
