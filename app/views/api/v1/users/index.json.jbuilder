json.users @users.each do |user|
  json.id user.id
  json.name user.name
  json.username user.username
  json.role user.role
  json.retail_chain user.retail_chain.name
end

json.pagination @pagination
