json.user do
  json.id    @user.id
  json.name  @user.name
  json.retail_chain @user.retail_chain.name
end