json.visitor do
  json.id    @visitor.id
  json.name  @visitor.name
  json.retail_chain @visitor.retail_chain.name
end