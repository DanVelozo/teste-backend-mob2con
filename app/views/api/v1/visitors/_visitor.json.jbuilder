json.visitors visitors.each do |visitor|
  json.id visitor.id
  json.name visitor.name
  json.retail_chain visitor.retail_chain.name
end

json.total_visitors @visitors.count

json.pagination @pagination