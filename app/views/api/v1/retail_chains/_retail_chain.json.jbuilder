json.retail_chains retail_chains.each do |retail_chain|
  json.id retail_chain.id
  json.name retail_chain.name
  json.cnpj retail_chain.cnpj
  json.total_visitors retail_chain.total_visitors
end

json.total_retal_chains @retail_chains.count

json.pagination @pagination