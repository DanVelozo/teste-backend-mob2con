json.retail_chain do
  json.id   @retail_chain.id
  json.name @retail_chain.name
  json.cnpj @retail_chain.cnpj
end