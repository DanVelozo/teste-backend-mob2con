class User < ApplicationRecord
  has_secure_password

  validates :name,     presence: true
  validates :username, presence: true, uniqueness: true
  validates :role,     presence: true

  belongs_to :retail_chain

  searchkick word_start: [:name]

  def to_token_payload
    { sub: id, username: username }
  end

  def self.from_token_request request
    username = request.params["auth"] && request.params["auth"]["username"]
    self.find_by username: username
  end

  def search_data
    { name: name }
  end
end
