class VisitorEntry < ApplicationRecord
  belongs_to :retail_chain
  belongs_to :visitor

  validates :checkin_at, presence: true
end