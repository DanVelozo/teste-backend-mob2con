class Api::V1::VisitorsController < ApplicationController
  include Api::V1::Concerns::Authorization
  include Api::V1::Concerns::Pagination

  before_action :authenticate_user
  before_action :set_visitor, only: %i[show update destroy]
  before_action :set_retail_chain, except: %i[destroy]
  before_action :check_token_blacklist
  before_action :authorize_as_admin

  def index
    if params[:q].present?
      @visitors = Visitor.search params[:q]

      render json: { errors: 'Visitor not found' }, status: :not_found if @visitors.results.empty?
    else
      @visitors = Visitor.all
    end

    @visitors

    @pagination = pagination_meta(Kaminari.paginate_array(@visitors).page(params[:page] ? params[:page].to_i : 1))
  end

  def show
  end

  def create
    @retail_chain.visitors.create!(visitor_params)

    if @retail_chain.save
      render json: {
        message: "Visitor #{@retail_chain.visitors.last.name} successfully created!",
        id: @retail_chain.visitors.last.id },
        status: :created
    end
  end

  def update
    if @visitor.update(visitor_params)
      render json: {
        message: "Visitor #{@visitor.name} successfully updated!",
        id: @visitor.id },
        status: :ok
    end
  end

  def destroy
    @visitor.destroy

    render json: {
      message: "Visitor #{@visitor.name} successfully removed!",
      id: @visitor.id }
  end

  private

    def set_visitor
      @visitor = Visitor.find(params[:id])
    end

    def set_retail_chain
      @retail_chain = RetailChain.find(params[:retail_chain_id])
    end

    def visitor_params
      params.permit(:name, :visitor_avatar)
    end
end
