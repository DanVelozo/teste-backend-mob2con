class Api::V1::UserTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token, raise: false

  def logout
    token = request.env["HTTP_AUTHORIZATION"]
    BlacklistToken.create!(token: token)

    render json: {
      message: "Successfully Logged Out" },
      status: :ok
  end
end

