class Api::V1::VisitorEntriesController < ApplicationController
  include Api::V1::Concerns::Authorization
  include Api::V1::Concerns::Pagination

  before_action :authenticate_user
  before_action :set_retail_chain
  before_action :set_visitor
  before_action :set_visitor_entry, only: %i[show check_out]
  before_action :check_token_blacklist
  before_action :authorize_as_admin, only: %i[index show]

  def index
    @visitor_entries = @visitor.visitor_entries.all

    @pagination = pagination_meta(Kaminari.paginate_array(@visitor_entries).page(params[:page] ? params[:page].to_i : 1))
  end

  def show
  end

  def check_in
    visitor_entry = @visitor
                     .visitor_entries
                     .create!(visitor_entry_params
                     .merge!(retail_chain_id: @retail_chain.id))

    if visitor_entry.save
      render json: {
        message: "Visitor entry successfully created!",
        id: visitor_entry.id },
        status: :created
    end
  end

  def check_out
    if @visitor_entry.update(visitor_entry_params)
      render json: {
        message: "Visitor entry successfully updated!",
        id: @visitor_entry.id },
        status: :ok
    end
  end

  private

  def set_retail_chain
    @retail_chain = RetailChain.find(params[:retail_chain_id])
  end

  def set_visitor
    @visitor = @retail_chain.visitors.find_by(id: params[:visitor_id])
  end

  def set_visitor_entry
    @visitor_entry = @visitor.visitor_entries.find(params[:entry_id])
  end

  def visitor_entry_params
    params.require(:visitor_entry).permit(:checkin_at, :checkout_at, :notes, retail_chain_attributes: [:id, :_destroy])
  end
end
