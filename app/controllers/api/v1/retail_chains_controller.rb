class Api::V1::RetailChainsController < ApplicationController
  include Api::V1::Concerns::Authorization
  include Api::V1::Concerns::Pagination

  before_action :authenticate_user
  before_action :set_retail_chain, only: %i[show update destroy]
  before_action :check_token_blacklist
  before_action :authorize_as_admin

  def index
    if params[:q].present?
      @retail_chains = RetailChain.search params[:q]

      render json: {
        message: 'Retail Chain not found' } if @retail_chains.results.empty?
    else
      @retail_chains = RetailChain.all
    end

    @retail_chains

    @pagination = pagination_meta(Kaminari.paginate_array(@retail_chains).page(params[:page] ? params[:page].to_i : 1))
  end

  def show
  end

  def create
    retail_chain = RetailChain.new(retail_chain_params)

    if retail_chain.save
      render json: {
        message: "Retail Chain #{retail_chain.name} successfully created!",
        id: retail_chain.id },
        status: :created
    end
  end

  def update
    if @retail_chain.update(retail_chain_params)
      render json: {
        message: "Retail Chain #{@retail_chain.name} successfully updated!",
        id: @retail_chain.id },
        status: :ok
    end
  end

  def destroy
    @retail_chain.destroy
    render json: {
      message: "Retail Chain #{@retail_chain.name} successfully removed!",
      id: @retail_chain.id }
  end

  private

    def set_retail_chain
      @retail_chain = RetailChain.find(params[:id])
    end

    def retail_chain_params
      params.permit(
        :name,
        :cnpj,
        :q
      )
    end
end
