module Api::V1::Concerns::Authorization
  extend ActiveSupport::Concern

  def authorize_as_admin
    if current_user.present? && current_user.role == "user"
      render json: { errors: "Not Allowed to Access this Resource" }, status: :unauthorized
    end
  end

  def check_token_blacklist
    token = request.env["HTTP_AUTHORIZATION"]

    return if token.empty?

    black_list_token = BlacklistToken.find_by(token: token)

    render json: { error: "Not Allowed to Access this Resource" }, status: :unauthorized if black_list_token.present?
  end
end