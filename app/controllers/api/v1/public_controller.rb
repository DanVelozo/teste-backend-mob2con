class Api::V1::PublicController < Api::V1::ApiController
  include Api::V1::Concerns::Pagination

  def index
    @retail_chains = RetailChain.includes(:visitors).page(params[:page] ? params[:page].to_i : 1)
    @pagination = pagination_meta(@retail_chains)
  end
end
