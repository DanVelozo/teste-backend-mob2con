Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      get '/retail_chains/public', to: 'public#index'

      resources :retail_chains do
        resources :users
        resources :visitors do
          get '/entries',                    to: 'visitor_entries#index'
          get '/entries/:entry_id',          to: 'visitor_entries#show'
          post '/entries/checkin',           to: 'visitor_entries#check_in'
          put '/entries/:entry_id/checkout', to: 'visitor_entries#check_out', as: :visitor_entry_checkout
          delete '/entries/:entry_id',       to: 'visitor_entries#destroy'
        end
      end

      post '/login',  to: 'user_token#create'
      post '/logout', to: 'user_token#logout'
    end
  end
end
