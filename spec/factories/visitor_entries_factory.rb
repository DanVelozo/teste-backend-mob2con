FactoryBot.define do
  factory :visitor_entry do
    checkin_at { DateTime.current }
    notes      { 'Observações XYZ' }
  end
end
