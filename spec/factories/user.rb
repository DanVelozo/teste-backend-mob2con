FactoryBot.define do
  factory :user do
    name     { 'Test User' }
    username { 'test_user' }
    password { '123456' }
    role     { 'user' }

    factory :admin do
      name     { 'Test Admin' }
      username { 'test_admin' }
      role     { 'admin' }
    end
  end
end