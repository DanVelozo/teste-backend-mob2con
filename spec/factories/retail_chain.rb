FactoryBot.define do
  factory :retail_chain do
    name { 'BIG Supermercado' }
    cnpj { '12345678912345' }
  end
end