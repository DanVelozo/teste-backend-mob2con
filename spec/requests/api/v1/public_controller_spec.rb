require "rails_helper"

RSpec.describe Api::V1::PublicController, type: :request do
  let!(:retail_chain) { FactoryBot.create(:retail_chain) }
  let!(:visitor_1)    { FactoryBot.create(:visitor, retail_chain: retail_chain) }
  let!(:visitor_2)    { FactoryBot.create(:visitor, retail_chain: retail_chain) }

  describe 'GET /v1/retail_chains/public' do
    it 'returns all retail chains' do
      get api_v1_retail_chains_public_url

      expect(response.code).to eq "200"
      expect(response).to be_successful
      expect(json_response[:total_retail_chains]).to eq(1)
      expect(json_response[:retail_chains].dig(0, :total_visitors)).to eq(2)
    end
  end
end