require "rails_helper"

RSpec.describe Api::V1::VisitorsController, type: :request do
  context 'Admin' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:admin) { FactoryBot.create(:admin, retail_chain: retail_chain) }
    let!(:visitor) { FactoryBot.create(:visitor, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains/:id/visitors' do
      it 'returns all visitors' do
        get api_v1_retail_chain_visitors_url(retail_chain.id), headers: auth_headers(admin)

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end

      it 'returns visitors by id' do
        get api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id), headers: auth_headers(admin)

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end

      it 'should not return visitor if id does not exists' do
        get api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id = 30334438), headers: auth_headers(admin)

        expect(response.code).to eq "404"
        expect(json_response[:errors]).to eq "Couldn't find Visitor with 'id'=#{visitor.id}"
      end
    end

    describe 'POST /v1/retail_chains/:id/visitors' do
      let(:new_visitor) {
        { name: "Carluxo" }
      }

      it 'create retail chains' do
        post api_v1_retail_chain_visitors_url(retail_chain.id), headers: auth_headers(admin), params: new_visitor

        expect(response).to be_successful
        expect(response.code).to eq "201"
        expect(json_response[:message]).to eq "Visitor Carluxo successfully created!"
      end
    end

    describe 'PUT /v1/retail_chains/:id/visitors/:id' do
      it 'update retail chains' do
        put api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id), headers: auth_headers(admin), params: { name: "New name"}

        expect(response).to be_successful
        expect(response.code).to eq "200"
        expect(json_response[:message]).to eq "Visitor New name successfully updated!"
      end
    end

    describe 'DELETE /v1/retail_chains/:id/visitors/:id' do
      it 'delete retail chains' do
        delete api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id), headers: auth_headers(admin)

        expect(response).to be_successful
        expect(response.code).to eq "200"
        expect(json_response[:message]).to eq "Visitor João da Silva successfully removed!"
      end
    end
  end

  context 'User' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }
    let!(:visitor) { FactoryBot.create(:visitor, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains/:id/visitors' do
      it 'should not return all visitors' do
        get api_v1_retail_chain_visitors_url(retail_chain.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
      end

      it 'should not returns visitor by id' do
        get api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
      end

      it 'should not return visitor if id does not exists' do
        get api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id = 30334438), headers: auth_headers(user)

        expect(response.code).to eq "404"
        expect(json_response[:errors]).to eq "Couldn't find Visitor with 'id'=#{visitor.id}"
      end

      it 'should not return visitor by query' do
        get api_v1_retail_chain_visitors_url(retail_chain.id, visitor.id), headers: auth_headers(user), params: { q: "João" }

        expect(response.code).to eq "401"
      end
    end

    describe 'POST /v1/retail_chains/:id/visitors' do
      let(:new_visitor) {
        { name: "Carluxo" }
      }

      it 'should not create visitor' do
        post api_v1_retail_chain_visitors_url(retail_chain.id), headers: auth_headers(user), params: new_visitor

        expect(response.code).to eq "401"
      end
    end

    describe 'PUT /v1/retail_chains/:id/visitors/:id' do
      it 'should not update retail chains' do
        put api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id), headers: auth_headers(user), params: { name: "New name"}

        expect(response.code).to eq "401"
      end
    end

    describe 'DELETE /v1/retail_chains/:id/visitors/:id' do
      it 'should not delete retail chains' do
        delete api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
      end
    end
  end

  context 'Without Token' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }
    let!(:visitor) { FactoryBot.create(:visitor, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains/:id/visitors' do
      it 'should not return all visitors' do
        get api_v1_retail_chain_visitors_url(retail_chain.id), headers: nil

        expect(response.code).to eq "401"
      end

      it 'should not return visitors by id' do
        get api_v1_retail_chain_visitor_url(visitor.id, retail_chain.id), headers: nil

        expect(response.code).to eq "401"
      end

      it 'should not return visitors by query' do
        get api_v1_retail_chain_visitor_url(visitor.id, retail_chain.id), headers: nil

        expect(response.code).to eq "401"
      end
    end

    describe 'POST /v1/retail_chains/:id/visitors' do
      let(:new_visitor) {
        { name: "Carluxo" }
      }

      it 'should not create visitor' do
        post api_v1_retail_chain_visitors_url(retail_chain.id), headers: nil, params: new_visitor

        expect(response.code).to eq "401"
      end
    end

    describe 'PUT /v1/retail_chains/:id/visitors/:id' do
      it 'should not update visitor' do
        put api_v1_retail_chain_visitor_url(visitor.id, retail_chain.id), headers: nil, params: { name: "New name"}

        expect(response.code).to eq "401"
      end
    end

    describe 'DELETE /v1/retail_chains/:id/visitors/:id' do
      it 'should not delete visitor' do
        delete api_v1_retail_chain_visitor_url(retail_chain.id, visitor.id), headers: nil

        expect(response.code).to eq "401"
      end
    end
  end
end