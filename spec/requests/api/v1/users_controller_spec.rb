require "rails_helper"

RSpec.describe Api::V1::UsersController, type: :request do

  context 'Admin' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:admin) { FactoryBot.create(:admin, retail_chain: retail_chain) }
    let!(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains/:id/users' do
      it 'returns all users' do
        get api_v1_retail_chain_users_url(retail_chain.id), headers: auth_headers(admin)

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end

      it 'returns user by id' do
        get api_v1_retail_chain_user_url(retail_chain.id, user.id), headers: auth_headers(admin)

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end

      it 'should not return user if id does not exists' do
        get api_v1_retail_chain_user_url(retail_chain.id, user.id = 30334438), headers: auth_headers(admin)

        expect(response.code).to eq "404"
        expect(json_response[:errors]).to eq "Couldn't find User with 'id'=#{user.id}"
      end

      it 'returns users by query' do
        get api_v1_retail_chain_users_url(retail_chain.id, user.id), headers: auth_headers(admin), params: { q: "José" }

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end
    end

    describe 'POST /v1/retail_chains/:id/users' do
      let(:new_user) {
        { name: "Queiroz", username: "orange123", password: '12345' }
      }

      it 'create users' do
        post api_v1_retail_chain_users_url(retail_chain.id), headers: auth_headers(admin), params: new_user

        expect(response).to be_successful
        expect(response.code).to eq "201"
        expect(json_response[:message]).to eq "User Queiroz successfully created!"
      end
    end

    describe 'PUT /v1/retail_chains/:id/users/:id' do
      it 'update user' do
        put api_v1_retail_chain_user_url(retail_chain.id, user.id), headers: auth_headers(admin), params: { name: "New name"}

        expect(response).to be_successful
        expect(response.code).to eq "200"
        expect(json_response[:message]).to eq "User New name successfully updated!"
      end
    end

    describe 'DELETE /v1/retail_chains/:id/users/:id' do
      it 'delete user' do
        delete api_v1_retail_chain_user_url(retail_chain.id, user.id), headers: auth_headers(admin)

        expect(response).to be_successful
        expect(response.code).to eq "200"
        expect(json_response[:message]).to eq "User Test User successfully removed!"
      end
    end
  end

  context 'User' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains/:id/users' do
      it 'should not return users' do
        get api_v1_retail_chain_users_url(retail_chain.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
      end

      it 'should not return user by id' do
        get api_v1_retail_chain_user_url(retail_chain.id, user.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
      end

      it 'should not return user by query' do
        get api_v1_retail_chain_users_url(retail_chain.id), headers: auth_headers(user), params: { q: "João" }

        expect(response.code).to eq "401"
      end
    end

    describe 'POST /v1/retail_chains/:id/users' do
      let(:new_user) {
        { name: "Jair", username: "biroliro", password: '12345' }
      }

      it 'should not create user' do
        post api_v1_retail_chain_users_url(retail_chain.id), headers: auth_headers(user), params: new_user

        expect(response.code).to eq "401"
      end
    end

    describe 'PUT /v1/retail_chains/:id/users/:id' do
      it 'should not update user' do
        put api_v1_retail_chain_user_url(retail_chain.id, user.id), headers: auth_headers(user), params: { name: "New name"}

        expect(response.code).to eq "401"
      end
    end

    describe 'DELETE /v1/retail_chains/:id/users/:id' do
      it 'should not delete retail chains' do
        delete api_v1_retail_chain_user_url(retail_chain.id, user.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
      end
    end
  end

  context 'Without Token' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }
    let!(:visitor) { FactoryBot.create(:visitor, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains/:id/users' do
      it 'should not return users' do
        get api_v1_retail_chain_users_url(retail_chain.id), headers: nil

        expect(response.code).to eq "401"
      end

      it 'should not return users by id' do
        get api_v1_retail_chain_user_url(retail_chain.id, visitor.id), headers: nil

        expect(response.code).to eq "401"
      end

      it 'should not return users by query' do
        get api_v1_retail_chain_user_url(retail_chain.id, visitor.id), headers: nil

        expect(response.code).to eq "401"
      end
    end

    describe 'POST /v1/retail_chains/:id/users' do
      let(:new_user) {
        { name: "Carluxo", username: 'carlinhos', password: '12345' }
      }

      it 'should not create user' do
        post api_v1_retail_chain_users_url(retail_chain.id), headers: nil, params: new_user

        expect(response.code).to eq "401"
      end
    end

    describe 'PUT /v1/retail_chains/:id/users/:id' do
      it 'should not update user' do
        put api_v1_retail_chain_user_url(retail_chain.id, user.id), headers: nil, params: { name: "New name"}

        expect(response.code).to eq "401"
      end
    end

    describe 'DELETE /v1/retail_chains/:id/users/:id' do
      it 'should not delete visitor' do
        delete api_v1_retail_chain_user_url(retail_chain.id, user.id), headers: nil

        expect(response.code).to eq "401"
      end
    end
  end
end