require "rails_helper"

RSpec.describe Api::V1::RetailChainsController, type: :request do

  context 'Admin' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let(:admin) { FactoryBot.create(:admin, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains' do
      it 'returns all retail chains' do
        get api_v1_retail_chains_url, headers: auth_headers(admin)

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end

      it 'returns retail chain by id' do
        get api_v1_retail_chain_url(retail_chain.id), headers: auth_headers(admin)

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end

      it 'should not return retail chain if id does not exists' do
        get api_v1_retail_chain_url(retail_chain.id = 30334438), headers: auth_headers(admin)

        expect(response.code).to eq "404"
        expect(json_response[:errors]).to eq "Couldn't find RetailChain with 'id'=#{retail_chain.id}"
      end

      it 'returns retail chain by query' do
        get api_v1_retail_chains_url, headers: auth_headers(admin), params: { q: retail_chain.name }

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end
    end

    describe 'POST /v1/retail_chains' do
      let(:new_retail_chain) {
        { name: "Sonda Supermercado", cnpj: "123456789" }
      }

      it 'create retail chains' do
        post api_v1_retail_chains_url, headers: auth_headers(admin), params: new_retail_chain

        expect(response).to be_successful
        expect(response.code).to eq "201"
        expect(json_response[:message]).to eq "Retail Chain Sonda Supermercado successfully created!"
      end
    end

    describe 'PUT /v1/retail_chains/:id' do
      it 'update retail chains' do
        put api_v1_retail_chain_url(retail_chain.id), headers: auth_headers(admin), params: { name: "New name"}

        expect(response).to be_successful
        expect(response.code).to eq "200"
        expect(json_response[:message]).to eq "Retail Chain New name successfully updated!"
      end
    end

    describe 'DELETE /v1/retail_chains/:id' do
      it 'delete retail chains' do
        delete api_v1_retail_chain_url(retail_chain.id), headers: auth_headers(admin)

        expect(response).to be_successful
        expect(response.code).to eq "200"
        expect(json_response[:message]).to eq "Retail Chain BIG Supermercado successfully removed!"
      end
    end
  end

  context 'User' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains' do
      it 'should not return all retail chains' do
        get api_v1_retail_chains_url, headers: auth_headers(user)

        expect(response.code).to eq "401"
        expect(json_response[:errors]).to eq "Not Allowed to Access this Resource"
      end

      it 'should not return retail chain by id' do
        get api_v1_retail_chain_url(retail_chain.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
        expect(json_response[:errors]).to eq "Not Allowed to Access this Resource"
      end
    end

    describe 'POST /v1/retail_chains' do
      let(:new_retail_chain) {
        { name: "Sonda Supermercado", cnpj: "123456789" }
      }

      it 'should not create retail chains' do
        post api_v1_retail_chains_url, headers: auth_headers(user), params: new_retail_chain

        expect(response.code).to eq "401"
        expect(json_response[:errors]).to eq "Not Allowed to Access this Resource"
      end
    end

    describe 'PUT /v1/retail_chains/:id' do
      it 'should not update retail chains' do
        put api_v1_retail_chain_url(retail_chain.id), headers: auth_headers(user), params: { name: "New name"}

        expect(response.code).to eq "401"
        expect(json_response[:errors]).to eq "Not Allowed to Access this Resource"
      end
    end

    describe 'DELETE /v1/retail_chains/:id' do
      it 'should not delete retail chains' do
        delete api_v1_retail_chain_url(retail_chain.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
        expect(json_response[:errors]).to eq "Not Allowed to Access this Resource"
      end
    end
  end

  context 'Without Token' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }

    describe 'GET /v1/retail_chains' do
      it 'should not return all retail chains' do
        get api_v1_retail_chains_url, headers: nil

        expect(response.code).to eq "401"
      end

      it 'should not return retail chain by id' do
        get api_v1_retail_chain_url(retail_chain.id), headers: nil

        expect(response.code).to eq "401"
      end
    end

    describe 'POST /v1/retail_chains' do
      let(:new_retail_chain) {
        { name: "Sonda Supermercado", cnpj: "123456789" }
      }

      it 'should not create retail chains' do
        post api_v1_retail_chains_url, headers: nil, params: new_retail_chain

        expect(response.code).to eq "401"
      end
    end

    describe 'PUT /v1/retail_chains/:id' do
      it 'should not update retail chains' do
        put api_v1_retail_chain_url(retail_chain.id), headers: nil, params: { name: "New name"}

        expect(response.code).to eq "401"
      end
    end

    describe 'DELETE /v1/retail_chains/:id' do
      it 'should not delete retail chains' do
        delete api_v1_retail_chain_url(retail_chain.id), headers: nil

        expect(response.code).to eq "401"
      end
    end
  end
end