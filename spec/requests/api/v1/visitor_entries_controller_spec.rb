require "rails_helper"

RSpec.describe Api::V1::VisitorEntriesController, type: :request do
  context 'Admin' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:admin) { FactoryBot.create(:admin, retail_chain: retail_chain) }
    let!(:visitor) { FactoryBot.create(:visitor, retail_chain: retail_chain) }
    let!(:visitor_entry) { FactoryBot.create(:visitor_entry, retail_chain: retail_chain, visitor: visitor) }

    describe 'GET /v1/retail_chains/:id/visitors/:id/entries' do
      it 'returns all visitor entries' do
        get api_v1_retail_chain_visitor_entries_url(retail_chain.id, visitor.id), headers: auth_headers(admin)

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end

      it 'returns visitor entries by id' do
        get api_v1_retail_chain_visitor_entries_url(retail_chain.id, visitor.id, visitor_entry.id), headers: auth_headers(admin)

        expect(response.code).to eq "200"
        expect(response).to be_successful
      end
    end

    describe 'POST /v1/retail_chains/:id/visitors/:id/entries/checkin' do
      let(:new_visitor_entry) {
        { checkin_at: DateTime.current }
      }

      it 'create visitor entry' do
        post api_v1_retail_chain_visitor_entries_checkin_url(retail_chain.id, visitor.id), headers: auth_headers(admin), params: {visitor_entry: new_visitor_entry }

        expect(response).to be_successful
        expect(response.code).to eq "201"
        expect(json_response[:message]).to eq "Visitor entry successfully created!"
      end
    end

    describe 'PUT /v1/retail_chains/:id/visitors/:id/entries/:entry_id/checkout' do
      let(:visitor_entry_checkout) {
        { checkout_at: DateTime.current }
      }

      it 'updates visitor entry' do
        put api_v1_retail_chain_visitor_visitor_entry_checkout_url(retail_chain.id, visitor.id, visitor_entry.id), headers: auth_headers(admin), params: { visitor_entry: visitor_entry_checkout }

        expect(response).to be_successful
        expect(response.code).to eq "200"
        expect(json_response[:message]).to eq "Visitor entry successfully updated!"
      end
    end
  end

  context 'User' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }
    let!(:visitor) { FactoryBot.create(:visitor, retail_chain: retail_chain) }
    let!(:visitor_entry) { FactoryBot.create(:visitor_entry, retail_chain: retail_chain, visitor: visitor) }

    describe 'GET /v1/retail_chains/:id/visitors/:id/entries' do
      it 'should not return visitor entries' do
        get api_v1_retail_chain_visitor_entries_url(retail_chain.id, visitor.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
      end

      it 'should not return visitor entries by id' do
        get api_v1_retail_chain_visitor_entries_url(retail_chain.id, visitor.id, visitor_entry.id), headers: auth_headers(user)

        expect(response.code).to eq "401"
      end
    end

    describe 'POST /v1/retail_chains/:id/visitors/:id/entries/checkin' do
      let(:new_visitor_entry) {
        { checkin_at: DateTime.current }
      }

      it 'create visitor entry' do
        post api_v1_retail_chain_visitor_entries_checkin_url(retail_chain.id, visitor.id), headers: auth_headers(user), params: {visitor_entry: new_visitor_entry }

        expect(response).to be_successful
        expect(response.code).to eq "201"
        expect(json_response[:message]).to eq "Visitor entry successfully created!"
      end
    end

    describe 'PUT /v1/retail_chains/:id/visitors/:id/entries/:entry_id/checkout' do
      let(:visitor_entry_checkout) {
        { checkout_at: DateTime.current }
      }

      it 'updates visitor entry' do
        put api_v1_retail_chain_visitor_visitor_entry_checkout_url(retail_chain.id, visitor.id, visitor_entry.id), headers: auth_headers(user), params: { visitor_entry: visitor_entry_checkout }

        expect(response).to be_successful
        expect(response.code).to eq "200"
        expect(json_response[:message]).to eq "Visitor entry successfully updated!"
      end
    end
  end

  context 'Without Token' do
    let!(:retail_chain) { FactoryBot.create(:retail_chain) }
    let!(:user) { FactoryBot.create(:user, retail_chain: retail_chain) }
    let!(:visitor) { FactoryBot.create(:visitor, retail_chain: retail_chain) }
    let!(:visitor_entry) { FactoryBot.create(:visitor_entry, retail_chain: retail_chain, visitor: visitor) }

    describe 'GET /v1/retail_chains/:id/visitors' do
      it 'should not return visitor entries' do
        get api_v1_retail_chain_visitor_entries_url(retail_chain.id, visitor.id), headers: nil

        expect(response.code).to eq "401"
      end

      it 'should not return visitor entries by id' do
        get api_v1_retail_chain_visitor_entries_url(retail_chain.id, visitor.id, visitor_entry.id), headers: nil

        expect(response.code).to eq "401"
      end
    end

    describe 'POST /v1/retail_chains/:id/visitors/:id/entries/checkin' do
      it 'should not create visitor entry' do
        post api_v1_retail_chain_visitor_entries_checkin_url(retail_chain.id, visitor.id), headers: nil

        expect(response.code).to eq "401"
      end
    end

    describe 'PUT /v1/retail_chains/:id/visitors/:id/entries/:entry_id/checkout' do
      it 'should not updates visitor entry' do
        put api_v1_retail_chain_visitor_visitor_entry_checkout_url(retail_chain.id, visitor.id, visitor_entry.id), headers: nil

        expect(response.code).to eq "401"
      end
    end
  end
end