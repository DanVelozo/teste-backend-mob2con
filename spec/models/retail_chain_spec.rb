require "rails_helper"

RSpec.describe RetailChain, type: :model do
  describe "Associations" do
    it { should have_many(:users) }
    it { should have_many(:visitors) }
  end

  describe "Validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:cnpj) }
  end

  describe "Attributes" do
    it "is valid with a name and cnpj" do
      user = RetailChain.new(
        name: "Test",
        cnpj: "12345678912345",
      )
      expect(user).to be_valid
    end

    it "is not valid without a name" do
      retail_chain = RetailChain.new(name: nil)
      expect(retail_chain).to_not be_valid
    end

    it "is not valid without a cnpj" do
      retail_chain = RetailChain.new(cnpj: nil)
      expect(retail_chain).to_not be_valid
    end
  end
end