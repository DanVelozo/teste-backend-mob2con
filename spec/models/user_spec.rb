require "rails_helper"

RSpec.describe User, type: :model do
  let!(:retail_chain) { FactoryBot.create(:retail_chain) }

  describe "Associations" do
    it { should belong_to(:retail_chain) }
  end

  describe "Validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:role) }
    it { should validate_uniqueness_of(:username) }
  end

  describe "Attributes" do
    it "is valid with a name, username role, password and retail_chain" do
      user = User.new(
        name: "Test User",
        username:  "testuser",
        role:      "user",
        password:  "123456",
        retail_chain: retail_chain
      )
      expect(user).to be_valid
    end

    it "is not valid without a name" do
      user = User.new(name: nil)
      expect(user).to_not be_valid
    end

    it "is not valid without a username" do
      user = User.new(
        name: "Test User",
        username:  nil,
        role:      "user",
        password:  "123456",
        retail_chain: retail_chain
        )
      expect(user).to_not be_valid
    end

    it "is not valid without a role" do
       user = User.new(
        name: "Test User",
        username:  'testuser',
        role:      nil,
        password:  "123456",
        retail_chain: retail_chain
        )
      expect(user).to_not be_valid
    end

    it "is not valid without a password" do
       user = User.new(
        name: "Test User",
        username:  'testuser',
        role:      'user',
        password:  nil,
        retail_chain: retail_chain
        )
      expect(user).to_not be_valid
    end
  end
end