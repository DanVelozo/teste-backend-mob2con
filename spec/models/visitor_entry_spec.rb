require "rails_helper"

RSpec.describe VisitorEntry, type: :model do
  let!(:retail_chain) { FactoryBot.create(:retail_chain) }
  let!(:visitor)      { FactoryBot.create(:visitor, retail_chain: retail_chain) }

  describe "Associations" do
    it { should belong_to(:retail_chain) }
    it { should belong_to(:visitor) }
  end

  describe "Validations" do
    it { should validate_presence_of(:checkin_at) }
  end

  describe "Attributes" do
    it "is valid with a ceckin_at, retail_chain and visitor" do
      visitor_entry = VisitorEntry.new(
        checkin_at: "21:30",
        visitor: visitor,
        retail_chain: retail_chain
      )
      expect(visitor_entry).to be_valid
    end

    it "is not valid without a checkin_at" do
      visitor_entry = VisitorEntry.new(checkin_at: nil)
      expect(visitor_entry).to_not be_valid
    end

    it "is not valid without a retail_chain" do
      visitor_entry = VisitorEntry.new(checkin_at: '21:30', retail_chain: nil)
      expect(visitor_entry).to_not be_valid
    end

    it "is not valid without a visitor" do
      visitor_entry = VisitorEntry.new(checkin_at: '21:30', retail_chain: retail_chain, visitor: nil)
      expect(visitor_entry).to_not be_valid
    end
  end
end